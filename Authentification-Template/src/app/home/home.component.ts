import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../User/Model/user.model';
import { UserService } from '../User/Service/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  UserModel;

  constructor(private router : Router , public userService : UserService) { }

  ngOnInit(): void {
 
      this.userService.getProfile().subscribe(
        res => {
          this.UserModel = res; 
      },
      err => {}
      ) 
    
  }

}
