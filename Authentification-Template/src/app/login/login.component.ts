import { Component, OnInit } from '@angular/core';
import { User } from '../User/Model/user.model';
import { UserService } from '../User/Service/user.service';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formModel : User = {
    Username : '',
    FullName : '',
    Email : '',
    Password : ''
  }

  constructor(public userService : UserService , private route : Router , private toastr : ToastrService) { }

  ngOnInit(): void {

    if(sessionStorage.getItem('token') != null )
        this.route.navigateByUrl('/home');

  }

  login(form : NgForm){
    this.userService.login(form.value).subscribe(
      (succ:any) => {
          sessionStorage.setItem('token',succ.token);
          this.route.navigateByUrl('/home');
      },
      err  => {
        if(err.status == 401)
           this.toastr.error("Username Or Password Not Correct !!" , "Authenticate");
      }
      
      )
  }

  

}
