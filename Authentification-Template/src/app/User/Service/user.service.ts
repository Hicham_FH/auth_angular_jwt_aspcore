import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../Model/user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  readonly URL = 'http://localhost:65017/api'

  constructor(private http : HttpClient) { }

  login(form){
    return this.http.post(this.URL + '/Authentication/Login' ,form );
  }

  getProfile(){
    return this.http.get<User>(this.URL + '/Profile');
  }

  
}
