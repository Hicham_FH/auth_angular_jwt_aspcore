﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Authentification_API.Data;
using Authentification_API.Models;
using Authentification_API.Settings;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Authentification_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ApplicationSettings _settings;
        private readonly ApplicationDbContext _db;

        public AuthenticationController(ApplicationDbContext db ,UserManager<ApplicationUser> userManager , SignInManager<ApplicationUser> signInManager ,
                                        IOptions<ApplicationSettings> settings)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _settings = settings.Value;
            _db = db;
        }


        [HttpPost]
        [Route("Register")]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            try
            {
                if (model == null)
                {
                    return NotFound();
                }
                if (ModelState.IsValid)
                {
                    if (EmailExist(model.Email))
                    {
                        return BadRequest("Email Existe Déja");
                    }
                    if (UserNameExist(model.Username))
                    {
                        return BadRequest("Username Existe Déja");
                    }

                    var user = new ApplicationUser
                    {
                        UserName = model.Username,
                        FullName = model.FullName,
                        Email = model.Email

                    };

                    var result = await _userManager.CreateAsync(user, model.Password);
                    return Ok(result);

                }
                return StatusCode(StatusCodes.Status400BadRequest);
            }
            catch (Exception ex)
            {
                throw ex;
            }


        }



        [HttpPost]
        [Route("Login")]
        public async Task<IActionResult> Login(LoginModel model)
        {
            var user = await _userManager.FindByNameAsync(model.Username);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var tokenHandler = new JwtSecurityTokenHandler();

                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]{
                    new Claim("UserID", user.Id.ToString()),

                }),
                    Issuer = "https://hichamfh",
                    Audience = "https://hichamfh",
                    Expires = DateTime.Now.AddDays(7),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_settings.JWT_Key)), SecurityAlgorithms.HmacSha512Signature),
                };

                var token = tokenHandler.CreateToken(tokenDescriptor);
                return Ok(new
                {
                    token = tokenHandler.WriteToken(token),
                    expiration = token.ValidTo
                });
            }
            return Unauthorized();

        }



        private bool UserNameExist(string username)
        {
            return _db.Users.Any(x => x.UserName == username);
        }

        private bool EmailExist(string email)
        {
            return _db.Users.Any(x => x.Email == email);
        }




    }


 }
