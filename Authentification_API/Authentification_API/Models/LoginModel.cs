﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentification_API.Models
{
    public class LoginModel
    {
        public String Username { get; set; }
        public String Password { get; set; }
    }
}
