﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Authentification_API.Models
{
    public class RegisterModel
    {
        [StringLength(256), Required]
        public String Username { get; set; }

        [Column(TypeName = "nvarchar(255)")]
        public String FullName { get; set; }

        [StringLength(256), Required]
        [RegularExpression(@"\w+\@\w+.com|\w+.net")]
        public String Email { get; set; }

        [Required]
        public String Password { get; set; }
    }
}
